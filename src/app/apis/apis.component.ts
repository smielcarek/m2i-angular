import { Component, OnInit } from '@angular/core';
import { ApisService } from '../apis.service';
import { Category } from '../category';
import { Entry, EntryModel } from '../entry';

@Component({
  selector: 'app-apis',
  templateUrl: './apis.component.html',
  styleUrls: ['./apis.component.css']
})
export class ApisComponent implements OnInit {
  apis : Entry[] = [];
  categories : Category[] = [];
  filter : string = '';
  constructor(private apiService : ApisService) { }

  ngOnInit(): void {
    this.apiService
      .getEntries(this.filter)
      .subscribe(
        (apisFromService) => {
          this.apis = apisFromService;
        })

    this.apiService.getCategories().subscribe(
      (categoriesFromService) => {
        this.categories = categoriesFromService;
      }
    )
  }

  onCategoryChange(event : Event){
    if(event.target instanceof HTMLSelectElement){
      this.apiService.getEntries(event.target.value).subscribe(
        (apisFromService) => {
          this.apis = apisFromService;
        })
    }
  }
}

