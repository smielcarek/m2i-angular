import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class MessagerieService {

  messagerieUrl = "https://data.snx.ovh/messages.php";

  constructor(private http : HttpClient) { }

  getMessages() : Observable<Message[]>{
    return this.http.get<Message[]>(this.messagerieUrl);
  }

  sendMessage(message : Message) : Observable<Message> {
    return this.http.post<Message>(this.messagerieUrl, message);
  }
}
