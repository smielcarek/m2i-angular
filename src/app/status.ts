export interface Status {
    response: string;
    type : "info" | "error" | "warning";
}
