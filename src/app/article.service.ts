import { Injectable } from '@angular/core';
import { Article } from './article';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ArticleService {
  articlesUrl = "http://data.snx.ovh/API.json";

  constructor(private http : HttpClient) { }

  getArticles() : Observable<Article[]> {
    return this.http.get<Article[]>(this.articlesUrl);
  }
}
