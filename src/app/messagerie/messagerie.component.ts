import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { mergeMap, Subscription, timer } from 'rxjs';
import { AuthService } from '../auth.service';
import { Message } from '../message';
import { MessagerieService } from '../messagerie.service';

@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.css']
})

export class MessagerieComponent implements OnInit {
  messagerie = new FormGroup({
    pseudo : new FormControl(
        (this.authService.isConnected()) ? {value : this.authService.getCurrentUser().user!.username, disabled : true }: '', 
        [ Validators.required, Validators.minLength(3) ]
      ),
    message : new FormControl('', [ Validators.required, Validators.minLength(3) ])
  });

  messages : Message[] = [];

  sub : Subscription = new Subscription();

  constructor(private msgService : MessagerieService, private authService : AuthService) { }

  ngOnInit(): void {
    const myTimer = timer(1000, 5000);

    this.sub = myTimer.pipe( 
      mergeMap(() => this.msgService.getMessages() )
    )
    .subscribe( message => this.messages = message);
  }

  onSubmit(){
    this.msgService.sendMessage(this.messagerie.value as Message).subscribe((message) =>{
        this.msgService.getMessages().subscribe(
            (messages) => {
              this.messages = messages;
            }
        );
      }
    );
    this.messagerie.controls['message'].reset(); 
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }
}
