import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AuthStatus } from './auth-status';
import { Status } from './status';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private curUser = new BehaviorSubject<AuthStatus>({
    connected : false
  });

  curUserObservable = this.curUser.asObservable()

  constructor(private http: HttpClient) { }

  register(user: User) : Observable<User | Status> {
      return this.http.post<User | Status>('https://data.snx.ovh/register.php', user);
  }
  
  login(user : User) : Observable<User | Status>{
      return this.http.post<User | Status>('https://data.snx.ovh/login.php',user, {withCredentials : true});
  }

  getUser() : Observable<User | Status>{
    return this.http.get< User | Status>('https://data.snx.ovh/login.php', {withCredentials : true});
  }

  logout() : Observable<Status>{
    return this.http.get<Status>('https://data.snx.ovh/logout.php', {withCredentials : true});
  }

  setAuthStatus(auth : AuthStatus){
    this.curUser.next(auth);
  }

  getCurrentUser() : AuthStatus{
    return this.curUser.value;
  }

  isUser(obj : any): obj is User{
    return "username" in obj;
  }
  isStatus(obj : any) : obj is Status{
    return "response" in obj;
  }
  isConnected() : boolean {
    return this.curUser.value.connected;
  }
}
