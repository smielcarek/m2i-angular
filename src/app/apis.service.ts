import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Category, CategoryModel } from './category';
import { Entry, EntryModel } from './entry';

@Injectable({
  providedIn: 'root'
})
export class ApisService {

  entries : Entry[] = [];

  constructor(private http : HttpClient) { }

  getEntries(filter : string = '') : Observable<Entry[]> {
    return new Observable<Entry[]>(subscriber =>{
      this.http.get<EntryModel>("https://api.publicapis.org/entries?Category="+filter).subscribe((results) => {
        subscriber.next(results.entries)
      })  
    })
  }

  getCategories() : Observable<Category[]> {
    return new Observable<Category[]>(subscriber =>{
      this.http.get<CategoryModel>("https://api.publicapis.org/categories").subscribe((results) => {
        let cats : Category[] = [];
        for(let i = 0; i < results.categories.length; i++)
          cats.push({name : results.categories[i]});

        subscriber.next(cats)
      })  
    })
  }
}
