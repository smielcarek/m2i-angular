import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { StatusService } from '../status.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user : FormGroup = new FormGroup({
    username : new FormControl('', [Validators.required]),
    password : new FormControl('', [Validators.required]),
    email : new FormControl('', [Validators.required])
  });

  constructor(private authService : AuthService, private statusService : StatusService) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.authService.register(this.user.value).subscribe( (user) => {
      if(this.authService.isUser(user))
        this.statusService.envoyerStatus({
          response : "Inscription effectuée",
          type : "info"
        });
      else
        this.statusService.envoyerStatus(user);
    })
  }
}
