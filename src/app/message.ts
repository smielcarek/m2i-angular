export interface Message {
    pseudo : string;
    message : string;
    date? : string;
}
