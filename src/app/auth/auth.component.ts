import { Component, OnInit } from '@angular/core';
import { Status } from '../status';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  curNotification : Status = { response : '', type:"info"};
  constructor() { }

  ngOnInit(): void {
  }

  notif(){
    this.curNotification = {
      response : "Oh lala un drame vient d'arriver !",
      type : "info"
    }
  }

  receiveCloseEven(close : boolean){
    this.curNotification = {
      response : "",
      type : "info"
    }
  }
}
