import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Status } from '../status';
import { StatusService } from '../status.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new FormGroup({
    email : new FormControl('', [Validators.required]),
    password : new FormControl('', [ Validators.required])
  })

  constructor(private authService : AuthService, private statusService : StatusService, private router : Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
      this.authService.login(this.user.value as User).subscribe((ruser) => {
        if(this.authService.isUser(ruser)){
          this.statusService.envoyerStatus({
            response : "Connexion effectuée",
            type : "info"
          });
          
          this.authService.setAuthStatus({
            connected : true,
            user : ruser
          });

          this.router.navigateByUrl('/messagerie');
          
          sessionStorage.setItem("connected", "true");
          sessionStorage.setItem("user", JSON.stringify(ruser));
        }
        else
        this.statusService.envoyerStatus(ruser);

      });
  }

}
