import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApisComponent } from './apis/apis.component';
import { ArticlesComponent } from './articles/articles.component';
import { AuthComponent } from './auth/auth.component';
import { ContactComponent } from './contact/contact.component';
import { HelloComponent } from './hello/hello.component';
import { IsLoggedGuard } from './is-logged.guard';
import { IsNotLoggedGuard } from './is-not-logged.guard';
import { MessagerieComponent } from './messagerie/messagerie.component';

const routes: Routes = [
  {path : '', component : HelloComponent},
  {path : 'articles', component : ArticlesComponent },
  {path : 'apis', component : ApisComponent},
  {path : 'contact', component: ContactComponent},
  {path : 'messagerie', component: MessagerieComponent, canActivate: [
    IsLoggedGuard
  ]},
  {path: 'login', loadChildren: () => import('./authentication/authentication.module').then( m => m.AuthenticationModule), component: AuthComponent, canLoad: [IsNotLoggedGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
